A little Docker demo
====

Overview
----

This repo is meant to provide a simple example use case for Docker. It includes two Python projects.
One could happily install and run either of the two Python projects on the host machine (i.e., not
in a container), but problems would arise if both were installed directly on the host machine. To
demonstrate that last bit, I provide a Dockerfile for a doomed container that would attempt to
install both of the Python projects.

Setup
----

Setup on the host machine is just installing docker. I have included scripts for doing so in CentOS:

```
$ ./setup/install_docker
$ ./setup/post_install_docker
```

Demonstration
----

### Project A

Population by canton. Build the image, run the container, and you should see
population info (after build output):

```
$ cd ./A
$ docker build -t project-a .
$ docker run --rm project-a
```

### Project B

Same as project A but with land area instead of population:

```
$ cd ./B
$ docker build -t project-b .
$ docker run --rm project-b
```

### Projects A + B

I have included Dockerfile.AB, which tries to run the dependency installation
scripts of both project A and project B. This build will fail:

```
$ docker build -t project-ab -f ./Dockerfile.AB .
```
