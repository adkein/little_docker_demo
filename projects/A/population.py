#!/usr/bin/env python

import json


d = json.load(open("/population/population.json", "r"))

for k, v in d.iteritems():
	print "Canton {} has a population of {}.".format(k, v)
