#!/usr/bin/env python

import json


d = json.load(open("/area/area.json", "r"))

for k, v in d.items():
	print("Canton {} has an area of {} square km.".format(k, v))
